//
//  Game.swift
//  Farkler
//
//  Created by Joseph Brust on 11/22/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

struct Game : Codable {
    var players: [Player]
    var rounds: Int
    var activeRound: Int
    var activePlayer: Int
    
    private enum CodingKeys: String, CodingKey {
        case players
        case rounds
        case activeRound
        case activePlayer
    }
    
    mutating func addPlayer(_ player: Player) {
        players.append(player)
    }
    
    mutating func addNewRound() {
        rounds += 1
        
        for (index, _) in players.enumerated() {
            players[index].addScore(0)
        }
    }
    
    mutating func setActiveRound(_ index: Int) {
        activeRound = index
    }
    
    mutating func setActivePlayer(_ index: Int) {
        activePlayer = index
    }
    
    mutating func updatePlayerScore(playerIndex: Int, scoreIndex: Int, newScore: Int) -> [Int] {
        return players[playerIndex].editScore(scoreIndex: scoreIndex, newScore: newScore)
    }
    
    func getTotalScoreForPlayer(playerIndex: Int) -> Int {
        return players[playerIndex].getTotalScore()
    }
    
    init() {
        players = []
        rounds = 1
        activeRound = 0
        activePlayer = 0
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        players = try values.decode([Player].self, forKey: .players)
        rounds = try values.decode(Int.self, forKey: .rounds)
        activeRound = try values.decode(Int.self, forKey: .activeRound)
        activePlayer = try values.decode(Int.self, forKey: .activePlayer)
    }
}
