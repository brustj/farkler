//
//  Player.swift
//  Farkler
//
//  Created by Joseph Brust on 11/22/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

import UIKit

struct Player : Codable {
    var name: String
    var scores: [Int]
    var hasOpened: Bool
    
    private enum CodingKeys: String, CodingKey {
        case name
        case scores
        case hasOpened
    }
    
    mutating func addScore(_ score: Int) {
        print("add score for \(name)")
        scores.append(score)
    }
    
    mutating func editScore(scoreIndex: Int, newScore: Int) -> [Int] {
        print("editing \(name)'s score #\(scoreIndex) from \(scores[scoreIndex]) to \(newScore)")
        
        var scoresToZero: [Int] = []
        
        //
        // need to add alert to make sure someone wants to edit a score if
        // it means that they will have not opened then
        //
        
        //
        // need to account for situation where someone could edit a score to
        // say 50 when they haven't opened yet
        //
        
        if newScore >= 500 && !hasOpened {
            print("player \(name) has opened")
            
            hasOpened = true

            scores[scoreIndex] = newScore
        } else if scores[scoreIndex] >= 500 && newScore < 500 {
            print("player has potentially changed an opening score, checking for previous scores >= 500")

            var playerHadAlreadyOpened = false;
            
            // check to see if player has an earlier score that's 500 or above
            for i in 0 ..< scoreIndex {
                if scores[i] >= 500 {
                    print("player has an earlier opening score, no need to adjust any other entries")
                    
                    playerHadAlreadyOpened = true
                    
                    scores[scoreIndex] = newScore
                }
            }
            
            if !playerHadAlreadyOpened {
                var playerHasOpened = false;
                
                scoresToZero.append(scoreIndex)
                
                scores[scoreIndex] = 0
                
                // player does not have an earlier opening score, check upwards
                for i in scoreIndex + 1 ..< scores.count {
                    print("checking \(scores[i]) for another open score")
                    
                    if scores[i] < 500 && !playerHasOpened {
                        scoresToZero.append(i)
                        
                        scores[i] = 0
                    } else {
                        print("above 500 score found, set playerHasOpened to true")
                        
                        playerHasOpened = true
                        
                        break
                    }
                }
                
                if !playerHasOpened {
                    print("player \(name) has no longer opened")
                    
                    hasOpened = false
                }
            }
        } else {
            scores[scoreIndex] = newScore
        }
        
        print(scores)
        print(scoresToZero)
        
        return scoresToZero
    }
    
    func getTotalScore() -> Int {
        return scores.reduce(0, +)
    }
    
    init(name: String) {
        self.name = name
        scores = [0]
        hasOpened = false
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        scores = try values.decode([Int].self, forKey: .scores)
        hasOpened = try values.decode(Bool.self, forKey: .hasOpened)
    }
}
