//
//  PlayerNames.swift
//  Farkler
//
//  Created by Joseph Brust on 11/28/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

struct PlayerList {
    var players: [Int: String]
    
    mutating func editPlayer(name: String, atIndex: Int) {
        players[atIndex] = name
    }
    
    mutating func removePlayer(atIndex: Int) {
        players.removeValue(forKey: atIndex)
    }
    
    func getPlayerList() -> [String] {
        let sortedPlayers = players.sorted(by: <)
        var playerList: [String] = []
        
        for (_, name) in sortedPlayers {
            playerList.append(name)
        }
        
        return playerList
    }
    
    init() {
        players = [Int: String]()
    }
}
