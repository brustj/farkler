//
//  PlayerEntryViewController.swift
//  Farkler
//
//  Created by Joseph Brust on 11/19/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

import UIKit

let DISABLED_ALPHA = CGFloat(0.35)

class PlayerEntryViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var playerNumbers: [UIView]!
    @IBOutlet var playerEntryViews: [UIView]!
    @IBOutlet var playerEntryLabels: [UITextField]!
    @IBOutlet var addPlayerButtons: [UIButton]!
    @IBOutlet var removePlayerButtons: [UIButton]!
    
    var playerList = PlayerList()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for playerNumber in playerNumbers {
            playerNumber.alpha = DISABLED_ALPHA
        }

        for playerEntryView in playerEntryViews {
            playerEntryView.backgroundColor = UIColor.init(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: DISABLED_ALPHA)
        }

        for playerEntryLabel in playerEntryLabels {
            if playerEntryLabel.tag > 2 {
                playerEntryLabel.isEnabled = false
            }
            
            playerEntryLabel.delegate = self
        }
        
        //
        // entry view and buttons get cut off on smaller screen heights
        // need to explore putting entire view in scroll view
        //
    }
    
    @IBAction func addPlayer(_ sender: UIButton) {
        for playerNumber in playerNumbers {
            if playerNumber.tag == sender.tag {
                playerNumber.alpha = 1.0
            }
        }

        for playerEntryView in playerEntryViews {
            if playerEntryView.tag == sender.tag {
                playerEntryView.backgroundColor = UIColor.init(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: 1.0)
            }
        }

        for playerEntryLabel in playerEntryLabels {
            if playerEntryLabel.tag == sender.tag {
                playerEntryLabel.isEnabled = true
                playerEntryLabel.becomeFirstResponder()
            }
        }
        
        for addPlayerButton in addPlayerButtons {
            if addPlayerButton.tag == sender.tag {
                addPlayerButton.isHidden = true
            }
        }
        
        for removePlayerButton in removePlayerButtons {
            if removePlayerButton.tag == sender.tag {
                removePlayerButton.isHidden = false
            }
        }
    }
    
    @IBAction func removePlayer(_ sender: UIButton) {
        for playerNumber in playerNumbers {
            if playerNumber.tag == sender.tag {
                playerNumber.alpha = DISABLED_ALPHA
            }
        }

        for playerEntryView in playerEntryViews {
            if playerEntryView.tag == sender.tag {
                playerEntryView.backgroundColor = UIColor.init(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: DISABLED_ALPHA)
            }
        }

        for playerEntryLabel in playerEntryLabels {
            if playerEntryLabel.tag == sender.tag {
                playerEntryLabel.isEnabled = false
            }
        }
        
        for addPlayerButton in addPlayerButtons {
            if addPlayerButton.tag == sender.tag {
                addPlayerButton.isHidden = false
            }
        }
        
        for removePlayerButton in removePlayerButtons {
            if removePlayerButton.tag == sender.tag {
                removePlayerButton.isHidden = true
            }
        }
        
        playerList.removePlayer(atIndex: sender.tag)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startButtonPressed(_ sender: UIButton) {
        let players: [String] = playerList.getPlayerList()
        
        if players.count > 0 {
            UserDefaults.standard.set(players, forKey: "Players")
            
            self.performSegue(withIdentifier: "SegueToScoreEntry", sender: self)
        } else {
            let alert = UIAlertController(title: "Whoops", message: "Please enter at least one (1) player name in order to start a game.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text == "" {
            playerList.removePlayer(atIndex: textField.tag)
        } else {
            playerList.editPlayer(name: textField.text!, atIndex: textField.tag)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
