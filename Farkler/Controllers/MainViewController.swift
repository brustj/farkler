//
//  ViewController.swift
//  Farkler
//
//  Created by Joseph Brust on 11/4/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var defaults = UserDefaults.standard
    var gameInProgress: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        // check for saved game
        gameInProgress = defaults.bool(forKey: "GameInProgress")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        gameInProgress = defaults.bool(forKey: "GameInProgress")

        // if no game is in progress, default rule alerts to true
        if !gameInProgress {
            defaults.set(true, forKey: "AutomaticRulesAlerts")
        }
    }
    
    @IBAction func newGameButtonPressed(_ sender: UIButton) {
        if !gameInProgress {
            defaults.removeObject(forKey: "SavedGame")
            self.performSegue(withIdentifier: "SegueToPlayerEntry", sender: self)
        } else {
            let alert = UIAlertController(title: "Game in progress", message: "Are you sure you want to start a new game? All current progress will be lost.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.defaults.removeObject(forKey: "SavedGame")
                self.defaults.set(true, forKey: "AutomaticRulesAlerts")
                
                self.performSegue(withIdentifier: "SegueToPlayerEntry", sender: self)
            }))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func resumeButtonPressed(_ sender: UIButton) {
        if gameInProgress {
            self.performSegue(withIdentifier: "SegueToScoreEntry", sender: self)
        } else {
            let alert = UIAlertController(title: "Whoops", message: "There are no active games being played at this time.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func officialRulesButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SegueToRules", sender: self)
    }
    
    @IBAction func unwindToMainMenu(_ unwindSegue: UIStoryboardSegue) {}
}

