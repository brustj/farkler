//
//  ScoreAreaViewController.swift
//  Farkler
//
//  Created by Joseph Brust on 11/19/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

import UIKit

class ScoreAreaViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scoreGridScrollView: UIScrollView!
    @IBOutlet weak var playerColStackView: UIStackView!
    @IBOutlet weak var buttonGroupBottomConstraint: NSLayoutConstraint!
    
    var defaults = UserDefaults.standard
    var autoRuleAlerts: Bool = true
    var playerNames: [String] = []
    var game: Game = Game()
    var playerCols: [UIStackView] = []
    var scoreInputs: [[UITextField]] = []
    var playerTotals: [UILabel] = []
    var shouldValidateOnEndEditing: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       print("will appear")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        autoRuleAlerts = defaults.bool(forKey: "AutomaticRulesAlerts")
        
        if scoreInputs.count > 0 {
            
            //
            // figure out why active round and player isn't carrying over between
            // saved games
            //
            
            print(1)
            forceFocusToTF(col: game.activePlayer, row: game.activeRound)
        }
   }
   
    override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
    
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
   }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("did load")
        
        // programmatically adding constraints to player col stack view
        playerColStackView.translatesAutoresizingMaskIntoConstraints = false
        playerColStackView.leadingAnchor.constraint(equalTo: scoreGridScrollView.leadingAnchor).isActive = true
        playerColStackView.trailingAnchor.constraint(equalTo: scoreGridScrollView.trailingAnchor).isActive = true
        playerColStackView.topAnchor.constraint(equalTo: scoreGridScrollView.topAnchor).isActive = true
        playerColStackView.bottomAnchor.constraint(equalTo: scoreGridScrollView.bottomAnchor).isActive = true
        playerColStackView.widthAnchor.constraint(equalTo: scoreGridScrollView.widthAnchor).isActive = true
        
        let savedGame = getSavedGameData()
        
        if savedGame != nil {
            game = (savedGame)!
            
            print(game)
            
            resumeGame()
        } else {
            playerNames = UserDefaults.standard.array(forKey: "Players") as! [String]
            
            startGame()
        }
    }
    
    func startGame() {
        for (index, playerName) in playerNames.enumerated() {
            scoreInputs.append([])
            
            // create new player and add to game
            game.addPlayer(Player(name: playerName))

            // create player's score column with name label and total label
            createPlayerCol(name: playerName)
            
            // add initial score entry field
            addScoreEntry(playerIndex: index, scoreIndex: 0, score: "---")
        }

        // auto force focus to first player's score entry
        forceFocusToTF(col: 0, row: 0)

        defaults.set(true, forKey: "GameInProgress")
    }
    
    func resumeGame() {
        for _ in 0..<game.players.count {
            // add score entry array for each player
            scoreInputs.append([])
        }
        
        for (playerIndex, player) in game.players.enumerated() {
            // create player's score column with name label and total label
            createPlayerCol(name: player.name)
            
            for (scoreIndex, score) in player.scores.enumerated() {
                // add score entry field
                
                //
                // need to figure out how to detemrine if 0 is a farkle or ---
                //
                
                addScoreEntry(playerIndex: playerIndex, scoreIndex: scoreIndex, score: String(score))
            }
            
            print(2)
            // update player's total score
            playerTotals[playerIndex].text = String(player.getTotalScore())
        }
        
        //
        // determine best entry point for resuming game
        // maybe find last entered score?
        //
        
        // add focus to first score entry
        forceFocusToTF(col: 0, row: game.rounds - 1)
    }
    
    func createPlayerCol(name: String) {
        // add vertical stack view to store player's score entry tfs
        let playerRowStackView = UIStackView()
        playerRowStackView.backgroundColor = .darkGray
        playerRowStackView.axis = .vertical
        playerRowStackView.alignment = .fill
        playerRowStackView.distribution = .fill
        playerColStackView.addArrangedSubview(playerRowStackView)
        
        // add spacer view to force tfs aligned to the top
        let spacerView = UIView()
        playerRowStackView.addArrangedSubview(spacerView)
        
        // create player name label
        let nameLabel = PlayerNameLabel()
        nameLabel.text = name.uppercased()
        nameLabel.font = UIFont(name: "Quicksand-Bold", size: 13.0)
        nameLabel.textAlignment = .center
        playerRowStackView.insertArrangedSubview(nameLabel, at: playerRowStackView.subviews.count - 1)
        
        //
        // add name labels to separate horizontal view so that they're persistent as the user scrolls through the scores
        //
        
        // create player total label
        let playerTotalLabel = ScoreTotalLabel()
        playerTotalLabel.text = "0"
        playerTotalLabel.font = UIFont(name: "Quicksand-SemiBold", size: 12.0)
        playerTotalLabel.textColor = .lightGray
        playerTotalLabel.textAlignment = .center
        playerRowStackView.insertArrangedSubview(playerTotalLabel, at: playerRowStackView.subviews.count - 1)
        
        // add underline to player total label
        let playerTotalUnderline = CALayer()
        playerTotalUnderline.frame = CGRect(x: 0.0, y: 0.0, width: playerColStackView.frame.size.width, height: 1.0)
        playerTotalUnderline.backgroundColor = UIColor.lightGray.cgColor
        playerTotalLabel.layer.addSublayer(playerTotalUnderline)
        playerTotalLabel.layer.masksToBounds = true
        
        // append player total label to array
        playerTotals.append(playerTotalLabel)
        
        // append player vertical stack view to array
        playerCols.append(playerRowStackView)
    }
    
    func addScoreEntry(playerIndex: Int, scoreIndex: Int, score: String) {
        let entryTF = scoreEntryTextField()
        entryTF.tag = scoreIndex
        entryTF.text = score == "0" ? "X" : score
        playerCols[playerIndex].insertArrangedSubview(entryTF, at: playerCols[playerIndex].subviews.count - 2)
        
        // disable tf from animating in on add
        UIView.performWithoutAnimation {
            playerCols[playerIndex].setNeedsLayout()
            playerCols[playerIndex].layoutIfNeeded()
        }
        
        // append score tf to array storage of all score tfs
        scoreInputs[playerIndex].append(entryTF)
    }
    
    func gotoNextScoreInput() {
        if game.activePlayer < game.players.count - 1 {
            // still in the same round, update active player only
            game.setActivePlayer(game.activePlayer + 1)
        } else {
            // on the last player for the round, move to first player in the next round
            game.setActivePlayer(0)
            game.setActiveRound(game.activeRound + 1)
            
            if game.activeRound == game.rounds {
                // create new round
                startNewRound()
            }
        }
        
        forceFocusToTF(col: game.activePlayer, row: game.activeRound)
    }
    
    func startNewRound() {
        game.addNewRound()
        
        // add new round of score entry tfs for each player
        for (index, player) in game.players.enumerated() {
            
            //
            // look to combine with addScoreEntry eventually
            //
            
            let newScoreEntryTF = scoreEntryTextField()
            
            newScoreEntryTF.tag = player.scores.count
            
            // add new tf to player's vertical stack view
            playerCols[index].insertArrangedSubview(newScoreEntryTF, at: playerCols[index].subviews.count - 2)
            
            // disable tf from animating in on add
            UIView.performWithoutAnimation {
                playerCols[index].setNeedsLayout()
                playerCols[index].layoutIfNeeded()
            }
            
            // add new tfs to score inputs array
            scoreInputs[index].append(newScoreEntryTF)
        }
    }
    
    //
    // button actions
    //
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
//        print("nextButtonPressed validate - player \(game.activePlayer)")
        
        // force flag to avoid double validation from textFieldShouldEndEditing
        shouldValidateOnEndEditing = false
        
        if validateScore(score: scoreInputs[game.activePlayer][game.activeRound].text!, playerIndex: game.activePlayer) {
            gotoNextScoreInput()
        }
    }
    
    @IBAction func farkleButtonPressed(_ sender: UIButton) {
        scoreInputs[game.activePlayer][game.activeRound].text = "X"
        
//        print("farkleButtonPressed validate - player \(game.activePlayer)")
        
        // force flag to avoid double validation from textFieldShouldEndEditing
        shouldValidateOnEndEditing = false
        
        gotoNextScoreInput()
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SegueToInGameMenu", sender: self)
    }
    
    @IBAction func unwindToScoreEntry(_ unwindSegue: UIStoryboardSegue) {}
    
    //
    // textfield delegate methods
    //
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "---" {
            textField.text = ""
        }
        
        // find active tf's position (row/col or round/player)
        let tfPos = getTFPos(textField)

        // set active round and player based on tf's position
        game.setActiveRound(tfPos.1)
        game.setActivePlayer(tfPos.0)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // only validate if a score is entered or the user didn't click on the next or farkle buttons
        if textField.text == "" || textField.text == "---" || !shouldValidateOnEndEditing {
            // reset flag
            shouldValidateOnEndEditing = true
            return true
        } else {
//            print("textFieldShouldEndEditing validate - player \(game.activePlayer)")
            return validateScore(score: textField.text!, playerIndex: game.activePlayer)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.text = "---"
        }
        
        if textField.text == "0" {
            textField.text = "X"
        }
        
        // convert entry to score as Int
        let newScore = Int(textField.text!) ?? 0
        // find tf's position (row/col or round/player)
        let tfPos = getTFPos(textField)
        
        // update player's score
        let scoresToZero = game.updatePlayerScore(playerIndex: tfPos.0, scoreIndex: tfPos.1, newScore: newScore)
        
        // update score views if player changed their opening score
        if scoresToZero.count > 0 {
            for i in 0 ..< scoresToZero.count {
                scoreInputs[tfPos.0][scoresToZero[i]].text = "X"
            }
        }
        
        // update player total
        playerTotals[tfPos.0].text = String(game.players[tfPos.0].getTotalScore())
        
        saveGame()
    }
    
    //
    // helper funcs
    //
    
    func scoreEntryTextField() -> UITextField {
        let tf = ScoreEntryTextField()
        tf.delegate = self
        tf.text = "---"
        tf.font = UIFont(name: "Quicksand-SemiBold", size: 12.0)
        tf.textAlignment = .center
        tf.keyboardType = .numberPad
        tf.keyboardAppearance = .default
        
        return tf
    }
    
    func getTFPos(_ tf: UITextField) -> (Int, Int) {
        for (colIndex, playerScores) in scoreInputs.enumerated() {
            for (rowIndex, entryTf) in playerScores.enumerated() {
                if tf == entryTf {
                    return (colIndex, rowIndex)
                }
            }
        }
        
        return (0, 0)
    }
    
    func forceFocusToTF(col: Int, row: Int) {
        scoreInputs[col][row].becomeFirstResponder()
    }
    
    func validateScore(score: String, playerIndex: Int) -> Bool {
        let scoreAsInt: Int = Int(score) ?? 0
        
        if score == "" || score == "---" {
            shouldValidateOnEndEditing = false
            let alert = UIAlertController(title: "Game alert", message: "Please enter a score before moving on to the next player.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
//            print("Please enter a score before moving on to the next player.")
            
            return false
        }
                
        if scoreAsInt % 50 != 0 {
            shouldValidateOnEndEditing = false
            let alert = UIAlertController(title: "Game alert", message: "Scores may only be entered in multiples of 50. Please re-enter player's score to continue.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
//            print("Scores may only be entered in multiples of 50. Please re-enter player's score to continue.")
            
            return false
        }
        
        if scoreAsInt > 0 && scoreAsInt < 500 && !game.players[playerIndex].hasOpened {
            shouldValidateOnEndEditing = false
            let alert = UIAlertController(title: "Game alert", message: "In order to start recording any amount of points, you must score at least 500 points in one turn.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
//            print("In order to start recording any amount of points, you must score at least 500 points in one turn.")
            
            return false
        }
        
        return true
    }
    
    func saveGame() {
        defaults.set(try? PropertyListEncoder().encode(game), forKey: "SavedGame")
    }
    
    func getSavedGameData() -> Game? {
        guard let gameData = defaults.object(forKey: "SavedGame") as? Data else { return nil }
        guard let savedGame = try? PropertyListDecoder().decode(Game.self, from: gameData) else { return nil }
        
        return savedGame
    }
    
    //
    // keyboard notification selectors
    //
    
    @objc func keyboardWillShowNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification: notification)
    }
    
//    @objc func keyboardWillHideNotification(notification: NSNotification) {
//        updateBottomLayoutConstraintWithNotification(notification: notification)
//    }
    
    func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        // update constraint on button group based on keyboard height
        buttonGroupBottomConstraint.constant = -keyboardEndFrame.height + 16.0
            
        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func removeBottomLayoutConstraint() {
        buttonGroupBottomConstraint.constant = 16.0
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
