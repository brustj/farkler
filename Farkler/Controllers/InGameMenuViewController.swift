//
//  InGameMenuViewController.swift
//  Farkler
//
//  Created by Joseph Brust on 11/19/19.
//  Copyright © 2019 Joseph Brust. All rights reserved.
//

import UIKit

class InGameMenuViewController: UIViewController {

    @IBOutlet weak var autoRulesSwitch: UISwitch!
    
    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        autoRulesSwitch.isOn = defaults.bool(forKey: "AutomaticRulesAlerts")

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func autoRulesSwitchChanged(_ sender: UISwitch) {
        defaults.set(sender.isOn, forKey: "AutomaticRulesAlerts")
    }
    
    @IBAction func viewRulesButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SegueToRules", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
